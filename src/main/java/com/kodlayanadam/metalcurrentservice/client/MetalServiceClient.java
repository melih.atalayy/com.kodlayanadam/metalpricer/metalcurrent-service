package com.kodlayanadam.metalcurrentservice.client;

import com.kodlayanadam.metalcurrentservice.dto.MetalPriceClientDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "metal-current-service",url = "https://www.goldapi.io/api")
public interface MetalServiceClient {

    @GetMapping("/{metalSymbol}/{currencyCode}")
    MetalPriceClientDto retrieveMetalPrice(
            @RequestHeader("x-access-token") String token,
            @PathVariable("metalSymbol") String metalSymbol,
            @PathVariable("currencyCode") String currencyCode
    );
}
