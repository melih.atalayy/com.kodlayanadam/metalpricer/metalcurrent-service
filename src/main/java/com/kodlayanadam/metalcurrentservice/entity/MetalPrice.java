package com.kodlayanadam.metalcurrentservice.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "metal_price", schema = "current", catalog = "metal")
public class MetalPrice {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    @Basic
    @Column(name = "fetch_time", nullable = true)
    private LocalDateTime fetchTime;
    @Basic
    @Column(name = "metal", nullable = true, length = 3)
    private String metal;
    @Basic
    @Column(name = "currency", nullable = true, length = 5)
    private String currency;
    @Basic
    @Column(name = "exchange", nullable = true, length = 15)
    private String exchange;
    @Basic
    @Column(name = "symbol", nullable = true, length = 25)
    private String symbol;
    @Basic
    @Column(name = "prev_close_price", nullable = true, precision = 0)
    private Double prevClosePrice;
    @Basic
    @Column(name = "open_price", nullable = true, precision = 0)
    private Double openPrice;
    @Basic
    @Column(name = "low_price", nullable = true, precision = 0)
    private Double lowPrice;
    @Basic
    @Column(name = "high_price", nullable = true, precision = 0)
    private Double highPrice;
    @Basic
    @Column(name = "open_time", nullable = true)
    private LocalDateTime openTime;
    @Basic
    @Column(name = "price", nullable = true, precision = 0)
    private Double price;
    @Basic
    @Column(name = "ch", nullable = true, precision = 0)
    private Double ch;
    @Basic
    @Column(name = "chp", nullable = true, precision = 0)
    private Double chp;
    @Basic
    @Column(name = "ask", nullable = true, precision = 0)
    private Double ask;
    @Basic
    @Column(name = "bid", nullable = true, precision = 0)
    private Double bid;
    @Basic
    @Column(name = "price_gram_24k", nullable = true, precision = 0)
    private Double priceGram24K;
    @Basic
    @Column(name = "price_gram_22k", nullable = true, precision = 0)
    private Double priceGram22K;
    @Basic
    @Column(name = "price_gram_21k", nullable = true, precision = 0)
    private Double priceGram21K;
    @Basic
    @Column(name = "price_gram_20k", nullable = true, precision = 0)
    private Double priceGram20K;
    @Basic
    @Column(name = "price_gram_18k", nullable = true, precision = 0)
    private Double priceGram18K;
    @Basic
    @Column(name = "price_gram_16k", nullable = true, precision = 0)
    private Double priceGram16K;
    @Basic
    @Column(name = "price_gram_14k", nullable = true, precision = 0)
    private Double priceGram14K;
    @Basic
    @Column(name = "price_gram_10k", nullable = true, precision = 0)
    private Double priceGram10K;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetalPrice that = (MetalPrice) o;
        return Objects.equals(id, that.id) && Objects.equals(fetchTime, that.fetchTime) && Objects.equals(metal, that.metal) && Objects.equals(currency, that.currency) && Objects.equals(exchange, that.exchange) && Objects.equals(symbol, that.symbol) && Objects.equals(prevClosePrice, that.prevClosePrice) && Objects.equals(openPrice, that.openPrice) && Objects.equals(lowPrice, that.lowPrice) && Objects.equals(highPrice, that.highPrice) && Objects.equals(openTime, that.openTime) && Objects.equals(price, that.price) && Objects.equals(ch, that.ch) && Objects.equals(chp, that.chp) && Objects.equals(ask, that.ask) && Objects.equals(bid, that.bid) && Objects.equals(priceGram24K, that.priceGram24K) && Objects.equals(priceGram22K, that.priceGram22K) && Objects.equals(priceGram21K, that.priceGram21K) && Objects.equals(priceGram20K, that.priceGram20K) && Objects.equals(priceGram18K, that.priceGram18K) && Objects.equals(priceGram16K, that.priceGram16K) && Objects.equals(priceGram14K, that.priceGram14K) && Objects.equals(priceGram10K, that.priceGram10K);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fetchTime, metal, currency, exchange, symbol, prevClosePrice, openPrice, lowPrice, highPrice, openTime, price, ch, chp, ask, bid, priceGram24K, priceGram22K, priceGram21K, priceGram20K, priceGram18K, priceGram16K, priceGram14K, priceGram10K);
    }
}
