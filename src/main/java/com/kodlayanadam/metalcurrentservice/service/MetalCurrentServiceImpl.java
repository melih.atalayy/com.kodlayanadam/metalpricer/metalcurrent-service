package com.kodlayanadam.metalcurrentservice.service;

import com.kodlayanadam.common.enums.ServiceEnum;
import com.kodlayanadam.exception.ServiceException;
import com.kodlayanadam.metalcurrentservice.client.MetalServiceClient;
import com.kodlayanadam.metalcurrentservice.dto.MetalPriceClientDto;
import com.kodlayanadam.metalcurrentservice.dto.MetalRequestDto;
import com.kodlayanadam.metalcurrentservice.dto.MetalResponseDto;
import com.kodlayanadam.metalcurrentservice.entity.MetalPrice;
import com.kodlayanadam.metalcurrentservice.mapping.MetalPriceMapper;
import com.kodlayanadam.metalcurrentservice.repository.MetalPriceRepository;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;

@Service("MetalCurrentService")
public class MetalCurrentServiceImpl implements MetalCurrentService{

    private final MetalPriceMapper priceMapper = Mappers.getMapper(MetalPriceMapper.class);
    private final String token;
    private final MetalServiceClient metalServiceClient;
    private final MetalPriceRepository repository;

    public MetalCurrentServiceImpl(
            @Value("${application.access.token}")String token,
            MetalServiceClient metalServiceClient,
            MetalPriceRepository repository) {
        this.token = token;
        this.metalServiceClient = metalServiceClient;
        this.repository = repository;
    }

    @Transactional
    @Override
    public MetalResponseDto getCurrentMetalPrice(MetalRequestDto request) {
        MetalResponseDto response;
        try{
            MetalPrice existingMetal = repository.findTop1ByOrderByFetchTimeDesc();
            if(existingMetal != null){
                response = existingMetalCase(request,existingMetal);
            }
            else {
                response = createNewMetalPrice(request);
            }

            return response;
        }catch (ServiceException e){
            throw new ServiceException(e.getMessage(), e.getServiceEnum());
        }
    }

    private MetalResponseDto existingMetalCase(MetalRequestDto request, MetalPrice existingMetal){
        MetalResponseDto response;
        if(checkLast1HourPrice(existingMetal.getFetchTime())){
            response = createNewMetalPrice(request);
        }
        else {
            response = priceMapper.mapInherit2MetalResponseDto(existingMetal);
        }
        return  response;
    }

    private boolean checkLast1HourPrice(LocalDateTime exist){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime end = exist.plusHours(1L);
        return now.isAfter(end);
    }

    private MetalPriceClientDto getMetalPrice(MetalRequestDto request){
        try {
            return metalServiceClient.retrieveMetalPrice(
                    token,
                    request.getMetalSymbol().toString(),
                    request.getCurrencyCode().toString()
            );
        }catch (RuntimeException e){
            throw new ServiceException("goldapi.io service call problem!",ServiceEnum.NOT_OK);
        }
    }

    private MetalResponseDto createNewMetalPrice(MetalRequestDto request){
        MetalPriceClientDto clientDto = getMetalPrice(request);
        MetalPrice newMetalPrice = priceMapper.map2MetalPrice(clientDto);
        repository.save(newMetalPrice);
        return priceMapper.map2MetalResponseDto(clientDto);
    }

}
