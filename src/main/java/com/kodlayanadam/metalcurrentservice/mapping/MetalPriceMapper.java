package com.kodlayanadam.metalcurrentservice.mapping;

import com.kodlayanadam.metalcurrentservice.dto.MetalPriceClientDto;
import com.kodlayanadam.metalcurrentservice.dto.MetalResponseDto;
import com.kodlayanadam.metalcurrentservice.entity.MetalPrice;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

@Mapper(componentModel = "spring")
public interface MetalPriceMapper {


    default MetalPrice map2MetalPrice(MetalPriceClientDto clientDto){
        return MetalPrice
                .builder()
                .fetchTime(LocalDateTime.ofInstant(Instant.ofEpochSecond(clientDto.getTimestamp()),TimeZone.getDefault().toZoneId()))
                .metal(clientDto.getMetal())
                .currency(clientDto.getCurrency())
                .exchange(clientDto.getExchange())
                .symbol(clientDto.getSymbol())
                .prevClosePrice(clientDto.getPrev_close_price())
                .openPrice(clientDto.getOpen_price())
                .lowPrice(clientDto.getLow_price())
                .highPrice(clientDto.getHigh_price())
                .openTime(LocalDateTime.ofInstant(Instant.ofEpochSecond(clientDto.getOpen_time()),TimeZone.getDefault().toZoneId()))
                .price(clientDto.getPrice())
                .ch(clientDto.getCh())
                .chp(clientDto.getChp())
                .ask(clientDto.getAsk())
                .bid(clientDto.getBid())
                .priceGram24K(clientDto.getPrice_gram_24k())
                .priceGram22K(clientDto.getPrice_gram_22k())
                .priceGram21K(clientDto.getPrice_gram_21k())
                .priceGram20K(clientDto.getPrice_gram_20k())
                .priceGram18K(clientDto.getPrice_gram_18k())
                .priceGram16K(clientDto.getPrice_gram_16k())
                .priceGram14K(clientDto.getPrice_gram_14k())
                .priceGram10K(clientDto.getPrice_gram_10k())
                .build();
    }

    @Mappings({
            @Mapping(source = "metal", target = "metal"),
            @Mapping(source = "currency", target = "currency"),
            @Mapping(source = "price", target = "price"),
            @Mapping(source = "price_gram_24k",target = "priceGram24K"),
            @Mapping(source = "price_gram_22k",target = "priceGram22K"),
            @Mapping(source = "price_gram_21k",target = "priceGram21K"),
            @Mapping(source = "price_gram_20k",target = "priceGram20K"),
            @Mapping(source = "price_gram_18k",target = "priceGram18K"),
            @Mapping(source = "price_gram_16k",target = "priceGram16K"),
            @Mapping(source = "price_gram_14k",target = "priceGram14K"),
            @Mapping(source = "price_gram_10k",target = "priceGram10K")
    })
    MetalResponseDto map2MetalResponseDto(MetalPriceClientDto clientDto);

    @InheritInverseConfiguration
    MetalResponseDto mapInherit2MetalResponseDto(MetalPrice metalPrice);
}
