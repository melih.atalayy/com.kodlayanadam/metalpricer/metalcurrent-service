package com.kodlayanadam.metalcurrentservice.service;

import com.kodlayanadam.metalcurrentservice.dto.MetalRequestDto;
import com.kodlayanadam.metalcurrentservice.dto.MetalResponseDto;

public interface MetalCurrentService {

    MetalResponseDto getCurrentMetalPrice(MetalRequestDto request);
}
