package com.kodlayanadam.metalcurrentservice.dto;

import com.kodlayanadam.common.dto.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MetalPriceClientDto extends BaseDto {

    private Long timestamp;
    private String metal;
    private String currency;
    private String exchange;
    private String symbol;
    private Double prev_close_price;
    private Double open_price;
    private Double low_price;
    private Double high_price;
    private Long open_time;
    private Double price;
    private Double ch;
    private Double chp;
    private Double ask;
    private Double bid;
    private Double price_gram_24k;
    private Double price_gram_22k;
    private Double price_gram_21k;
    private Double price_gram_20k;
    private Double price_gram_18k;
    private Double price_gram_16k;
    private Double price_gram_14k;
    private Double price_gram_10k;
}
