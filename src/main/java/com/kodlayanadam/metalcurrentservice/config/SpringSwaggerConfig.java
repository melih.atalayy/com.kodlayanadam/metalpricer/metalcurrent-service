package com.kodlayanadam.metalcurrentservice.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringSwaggerConfig {

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("metalcurrent")
                .pathsToMatch("/metal/current/**")
                .packagesToScan("com.kodlayanadam.metalcurrentservice.controller")
                .build();
    }

    @Bean
    public OpenAPI springMetalCurrentOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("Metalic Current API")
                        .description("Metalic Current Project API")
                        .version("1.0")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org"))
                );
    }
}
