package com.kodlayanadam.metalcurrentservice.repository;

import com.kodlayanadam.metalcurrentservice.entity.MetalPrice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MetalPriceRepository extends JpaRepository<MetalPrice,Long> {

    MetalPrice findTop1ByOrderByFetchTimeDesc();
}
