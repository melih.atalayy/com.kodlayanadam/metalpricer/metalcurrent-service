package com.kodlayanadam.metalcurrentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.kodlayanadam.**")
@EnableFeignClients
public class MetalcurrentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MetalcurrentServiceApplication.class, args);
    }

}
