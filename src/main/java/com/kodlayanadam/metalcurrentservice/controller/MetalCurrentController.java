package com.kodlayanadam.metalcurrentservice.controller;

import com.kodlayanadam.common.dto.ServiceRequest;
import com.kodlayanadam.common.dto.ServiceResponse;
import com.kodlayanadam.mapping.ServiceMapper;
import com.kodlayanadam.metalcurrentservice.dto.MetalRequestDto;
import com.kodlayanadam.metalcurrentservice.dto.MetalResponseDto;
import com.kodlayanadam.metalcurrentservice.service.MetalCurrentService;
import org.mapstruct.factory.Mappers;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/metal/current")
public class MetalCurrentController {
    private final MetalCurrentService service;
    private final ServiceMapper serviceMapper = Mappers.getMapper(ServiceMapper.class);
    public MetalCurrentController(MetalCurrentService service) {
        this.service = service;
    }

    @PostMapping("/metal-price")
    public ResponseEntity<ServiceResponse<MetalResponseDto>> getCurrentMetalPrice(@RequestBody ServiceRequest<MetalRequestDto> request){

        ServiceResponse<MetalResponseDto> response = serviceMapper.mapToSuccessResponse(service.getCurrentMetalPrice(request.getInput()));
        return  ResponseEntity.ok(response);
    }
}